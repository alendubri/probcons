# ProbCons SpinOff

Probabilistic Consistency-based Multiple Alignment of Amino Acid Sequences



## About the ProbCons project

NOTE: This is a complete quote of the original ProbCons [Web Page](http://probcons.stanford.edu/about.html)

ProbCons is a novel tool for generating multiple alignments of protein sequences. Using a combination of probabilistic modelling and consistency-based alignment techniques, ProbCons has achieved the highest accuracies of all alignment methods to date. On the BAliBASE benchmark alignment database, alignments produced by ProbCons show statistically significant improvement over current programs, containing an average of 7% more correctly aligned columns than those of T-Coffee, 11% more correctly aligned columns than those of CLUSTAL W, and 14% more correctly aligned columns than those of DIALIGN.

ProbCons was developed by Chuong Do in collaboration with Michael Brudno in the research group of Serafim Batzoglou, Department of Computer Science, Stanford University.

## Further information

The methods used in ProbCons are described in the following paper:

[Do, C.B., Mahabhashyam, M.S.P., Brudno, M., and Batzoglou, S. 2005. ProbCons : Probabilistic Consistency-based Multiple Sequence Alignment. Genome Research 15: 330-340.](http://www.genome.org/cgi/content/abstract/15/2/330)

## Download and compilation of this Package

This package was modified by [Ing Alberto Dubuc](https://www.linkedin.com/in/alendubri/) to be possible to compile inside WSL Linux. The Distribution used was Ubuntu 20.04.6 LTS.

If you have a clean Installation of WSL Ubuntu, most probably you don't have either git or the GCC tools needed to compile this package

First thing you must do is an update of your system. You must fetch the following commands in your command line terminal:

```bash
sudo apt-get update
```
Following By:
```bash
sudo apt-get upgrade
```

The last command could ask you if "Do you want to continue? [Y/n]". You simply type "Y" and press the enter key.

After that all packages will be upgraded.

### Installing GCC

```bash
sudo apt install build-essential
```

### Clone Git repository
```bash
git clone https://gitlab.com/alendubri/probcons.git
```
## Compilation

```bash
cd probcons
make
```
You will have 4 executables in the same directory as you have compiled the package:

- compare
- makegnuplot
- probcons
- project

The executable probcons is the required program. If these programs do not exist then the compilation have failed for some reason.

## Manual and Test Functionality

The ProbCons manual is a PDF in the main website. You can download it [here](http://probcons.stanford.edu/manual.pdf)

To test the functionality of probcons, there is an example test file with the same example as described in page 5 of the PDF manual. The example is the file test.txt. You can run probcons in the following way:

```bash
./probcons test.txt > output.txt
```

To look at the content of the output.txt file, you can use the more command, and you will see something like this:

```bash
more output.txt
>plas_horvu
D-VLLGANGGVLVFEPNDFSVKAGETITFKNNAGYPHNVVFDEDAVPSG-VD-VSKISQE
EYLTAPGETFSVTLTV---PGTYGFYCEPHAGAGMVGKVTV
>plas_chlre
--VKLGADSGALEFVPKTLTIKSGETVNFVNNAGFPHNIVFDEDAIPSG-VN-ADAISRD
DYLNAPGETYSVKLTA---AGEYGYYCEPHQGAGMVGKIIV
>plas_anava
--VKLGSDKGLLVFEPAKLTIKPGDTVEFLNNKVPPHNVVFDAALNPAKSADLAKSLSHK
QLLMSPGQSTSTTFPADAPAGEYTFYCEPHRGAGMVGKITV
>plas_proho
VQIKMGTDKYAPLYEPKALSISAGDTVEFVMNKVGPHNVIFDK--VPAG-ES-APALSNT
KLRIAPGSFYSVTLGT---PGTYSFYCTPHRGAGMVGTITV
>azup_achcy
VHMLNKGKDGAMVFEPASLKVAPGDTVTFIPTDK-GHNVETIKGMIPDG-AE-A------
-FKSKINENYKVTFTA---PGVYGVKCTPHYGMGMVGVVEV
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[GPL-3.0](https://choosealicense.com/licenses/gpl-3.0/)
